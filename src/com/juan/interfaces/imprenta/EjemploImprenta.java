package com.juan.interfaces.imprenta;

import com.juan.interfaces.imprenta.modelo.*;

import static com.juan.interfaces.imprenta.modelo.Genero.*;

public class EjemploImprenta {
    public static void main(String[] args) {

        CurriculoDos cv = new CurriculoDos("Juan Pablo", "Ingeniero de sistemas");
        cv.addExperiencia("- Foscal");
        cv.addExperiencia("- HTS");
        cv.addExperiencia("- Offimedicas");

        imprimirDos(cv);


        /*
        Curriculo cv = new Curriculo("Juan Pablo", "Ingeniero de sistemas", "Resumen laboral...");
        cv.addExperiencias("Java");
        cv.addExperiencias("Reactj");
        cv.addExperiencias("Laravel");
        cv.addExperiencias("Vuejs");

        Libro libro = new Libro("Erich Gamma", "Patron de diseños: Elem. Reusables POO", PROGRAMACION);

        libro.addPagina(new Pagina("Patron Singleton"))
                .addPagina(new Pagina("Patrón Observador"))
                .addPagina(new Pagina("Patrón Factory"))
                .addPagina(new Pagina("Compositor"));


        Informe informe = new Informe("Martin", "Angélica", "Estudio de microservicio");
        imprimir(cv);
        */
        //imprimir(informe);
        //imprimir(libro);

    }

    /*
    public static void imprimir(Hoja imprimible) {
        System.out.println(imprimible.imprimir());
    }
    */

    /*
    public static void imprimir(Imprimible imprimible) {
        System.out.println(imprimible.imprimir());
    }
     */

    public static void imprimirDos(Imprimible imprimible) {
        System.out.println(imprimible.imprimir());
    }

}
