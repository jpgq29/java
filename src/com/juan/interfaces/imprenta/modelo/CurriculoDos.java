package com.juan.interfaces.imprenta.modelo;

import java.util.ArrayList;
import java.util.List;

public class CurriculoDos implements Imprimible {
    private String persona;
    private String carrera;
    private List<String> experiencias;

    public CurriculoDos(String persona, String carrera) {
        this.persona = persona;
        this.carrera = carrera;
        this.experiencias = new ArrayList<>();
    }

    public void addExperiencia(String ex) {
        this.experiencias.add(ex);
    }

    public String imprimir() {
        StringBuilder sb = new StringBuilder("Persona: ");
        sb.append(this.persona)
                .append("\n")
                .append("Carrera :")
                .append(this.carrera)
                .append("\n");
        for (String ex : this.experiencias) {
            sb.append(ex);
            sb.append("\n");
        }
        return sb.toString();
    }
}

