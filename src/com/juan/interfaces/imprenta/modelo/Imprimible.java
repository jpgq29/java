package com.juan.interfaces.imprenta.modelo;

public interface Imprimible {
    public String imprimir();
}
