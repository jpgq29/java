package com.juan.interfaces.imprenta.modelo;

public enum Genero {
    DRAMA,
    ACCION,
    AVENTURA,
    TERROR,
    CIENCIA,
    FICCION,
    PROGRAMACION;
}
