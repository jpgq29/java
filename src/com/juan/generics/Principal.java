package com.juan.generics;

public class Principal {
    public static void main(String[] args) {
        Bolsa<Golosina> bolsa = new Bolsa<Golosina>(5);
        Golosina g = new Golosina("Super coco");
        Golosina g2 = new Golosina("Jet");
        Golosina g3 = new Golosina("Mani");

        bolsa.add(g);
        bolsa.add(g2);
        bolsa.add(g3);

        for (Golosina golosina : bolsa) {
            System.out.println(golosina.getNombre());
        }
    }
}
